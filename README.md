# The Conscientious Theme

**Conscientious** is an excessively semantic, fast,
and user-friendly website theme doing everything right™.

## Goals and concept

**Conscientious** was originally developed for the very specific
personal website needs of the author. However, some ideas
turned out to be quite useful.

Relying heavily on the concept of
[taxonomies](https://www.getzola.org/documentation/content/taxonomies/),
yet being designed to make for a good personal portfolio and
blog website theme, **Conscientious** is very opnioninated concerning
the structure of the website.
