+++
title = "Concepts and Ideas"
description = "About the opinionated ideas behind the Conscentious theme"

[extra]
icon = "fa-solid fa-lightbulb"
+++

**Conscientious** is a theme not like any other. While it is
supposed to be a theme primarily for private publications, i.e.
[blogging](@/scenarios/blogging/index.md), it uses
[taxonomies](https://www.getzola.org/documentation/content/taxonomies/)
excessively for structure, and also exposes them where it makes
sense.
