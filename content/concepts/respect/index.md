+++
title = "User respect"
description = "Conscientious respects the freedoms and rights of users"

[taxonomies]
concepts = ["Respect"]

[extra]
icon = "fa-regular fa-handshake"
+++

The "modern" web is full of tracking, external fonts,
consent banners and unnecessary JavaScript. While
Static Site Generators tend to be better in that regard,
many themes still overlook basic user freedom and
rights by depending on external libraries or fonts.

Conscientious promises to respect users' freedoms and
rights by:

* not dependeing on any external file after the build
* being entirely free and open, without compromise
* not having any hard-coded links to social media,
  code hosting platforms, or the like
  