+++
title = "Structure"
description = "Taxonomies are used as the main structure in websites based on Conscientious"

[taxonomies]
concepts = ["Structure"]

[extra]
icon = "fa-solid fa-folder-tree"
+++

Websites made with **Conscientious** are all about structure –
Conscientious does not know websites without inherent structure.
The base for this structure is the use of
[taxonomies](https://www.getzola.org/documentation/content/taxonomies/).

## It's taxonomies all the way down

The first rule of **Conscientious** is simple:

{% message(color="info") %}
Every page must have taxonomies, and every taxonomy must have a page.
{% end %}

For the average blogger, this might at first feel a bit
unintuitive, if not inconvenient. However, it give us some
handy benefits:

* Authors are forced to keep their content ordered, and
  consider the structure and relation to other content
  before posting
* The website becomes more than a linear series of posts
  (while it can still be read in a linear fashion if desiered)

## Technical consequences

{% message(color="warning") %}
This explanation is only a part of the documentation on how to
use the **Conscentious** theme.

For a complete overview, refer to the getting-started guide in
the [theme readme](https://codeberg.org/Natureshadow/zola-theme-conscientious/src/branch/main/README.md).
{% end %}

**Conscientious** relies on a clear mapping between taxonomies
and content files when building pages. Starting with the front page,
the theme iterates over known taxonomies and amends them with
information from a respective section or page.

Given a `taxonomies` config like the following…

```toml
taxonomies = [
    { name="technologies", feed=true },
    { name="customers", feed=true },
]
```

…**Conscientious** expects a content tree like this:

```plain
content/
│ customers/
│ ├ cool-company
│ │ └ index.md
│ ├ horrific-company.md
│ │ └ index.md
│ └ _index.md
│ technologies/
│ ├ css
│ │ └ index.md
│ ├ html.md
│ │ └ index.md
│ └ _index.md
└ _index.md
```

As can be seen, we have two *taxonomies* ("customers" and
"technologies"), and we have prepared two *terms* for each.

For those who have worked with taxonomies in Zola before,
having sections and pages for each might look unexpected –
that's because, unlike **Conscientious**, other themes do
not connect visual layout with taxonomies (they are merely
used for tagging posts).

**Conscientious**, however, will use the special sections
and pages to link visual content to the taxonomies:

* It will derive "display names" and descriptions from
  the `title` and `description` fields of the pages
* It will render a catalog of the taxonomies and terms
  on the frontpage, including titles, descriptions, and
  icons
* It will generate "cover pages" for each term, including
  the term's page content

So, each taxonomy kind's `_index.md` and each term's `index.md`
need a front-matter, consisting of at least:

```toml
title = "Technologies & Stacks"
description = "Technologies we use for our hipster products"

[extra]
icon = "fa-solid fa-microchip"
```

That, as has hopefully become clear, will take care of rendering
the provided texts everywhere they are used for user-facing
pages. Right now, you are looking at a term's `index.md` page.

{% message(color="info") %}
If you want to use additional taxonomies that are not mangled
into web pages by **Conscientious**, you can set their `render`
attribute to `false`.
{% end %}

{% message(color="warning") %}
Each term must contain at least one page to make it known
to Zola. If you need a term rendered before having a real
content page or article, add the term to the `taxonomies`
of the term's special `index.md`'s front-matter as a
placeholder.
{% end %}

## Adding content

Basically, pages and articles are categorized into taxonomies
just as always. Take care to place your pages outside the special
section directories used for your taxonomy.

When adding a page, add the relevant taxonomies
to their front-matter:

```toml
title = "How we made Big Bad Company happy with tons of JavaScript"
date = "2023-04-01"

[taxonomies]
customers = ["Big Bad Company"]
techonologies = ["JavaScript"]
```

**Conscientious** will now consider the page when generating
taxonomy pages. Doing so, it will make a distinction between
regular pages and articles (posts): All pages that have a `date`
are considered articles.

{% message(color="info") %}
It appears reasonable to structure articles and news posts into
a separate section, including the year and month, e.g.
`content/blog/2023/04/big-bad-company-js.md`. See the
[blogging scenario](@/scenarios/blogging/index.md) for more
thoughts on posting articles.
{% end %}

## Cross-references

One extension that **Conscientious** makes to Zola's concept
of taxonomies is that it scans for cross-references. That means
that for taxonomy pages, all other taxonomy terms used on
pages that are in *the current* taxonomy will be listed in
a panel as well.
