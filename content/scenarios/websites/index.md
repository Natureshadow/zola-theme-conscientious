+++
title = "Structured websites"
description = "Well-structured, catalog-like websites can be made with Conscientious"

[taxonomies]
scenarios = ["Websites"]

[extra]
icon = "fa-solid fa-sitemap"
+++
